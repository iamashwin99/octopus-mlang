# -*- coding: utf-8 mode: shell-script -*-

Test       : Water in Supercell
Program    : octopus
TestGroups : long-run, periodic_systems
Enabled    : Yes

Input: 06-h2o_pol_lr.01_gs.inp
match ; SCF convergence ; GREPCOUNT(static/info, 'SCF converged') ; 1
match ; Space group        ; GREPFIELD(out, 'Space group', 4) ; 25
match ; No. of symmetries  ; GREPFIELD(out, 'symmetries that can be used', 5)  ;   4
Precision: 8.60e-08
match ;   Total energy   ; GREPFIELD(static/info, 'Total       =', 3) ; -17.20893825
Precision: 4.54e-05
match ;   Eigenvalue 1   ; GREPFIELD(static/info, '1   --', 3) ; -0.907979
Precision: 5.00e-15
match ;   Dipole x       ; GREPFIELD(static/info, '<x>', 3) ; -1.3553999999999999e-15
Precision: 3.82e-05
match ;   Dipole y       ; GREPFIELD(static/info, '<y>', 3) ; 0.764809
Precision: 2.30e-14
match ;   Dipole z       ; GREPFIELD(static/info, '<z>', 3) ; 1.29268e-14

Input: 06-h2o_pol_lr.02_kdotp.inp
Precision: 7.66e-06
match ;   Inverse effective mass 1   ; LINEFIELD(kdotp/kpoint_1_1, 11, 3) ; 0.0015329999999999999
Precision: 6.78e-06
match ;   Inverse effective mass 2   ; LINEFIELD(kdotp/kpoint_1_1, 17, 3) ; 0.013554
Precision: 8.30e-06
match ;   Inverse effective mass 3   ; LINEFIELD(kdotp/kpoint_1_1, 23, 3) ; 0.016595
Precision: 2.05e-16
match ;   Inverse effective mass 4   ; LINEFIELD(kdotp/kpoint_1_1, 29, 3) ; 0.020474000000000003

Input: 06-h2o_pol_lr.03_emresp.inp
if(available libxc_fxc); then
  Precision: 5.16e-05
  match ;   Polarizability XX w=0.0     ; LINEFIELD(em_resp/freq_0.0000/alpha, 2, 1) ; 10.32113
  Precision: 5.15e-06
  match ;   Polarizability YY w=0.0     ; LINEFIELD(em_resp/freq_0.0000/alpha, 3, 2) ; 10.304387
  Precision: 5.11e-06
  match ;   Polarizability ZZ w=0.0     ; LINEFIELD(em_resp/freq_0.0000/alpha, 4, 3) ; 10.211826
  Precision: 5.32e-06
  match ;   Polarizability XX w=0.1     ; LINEFIELD(em_resp/freq_0.1000/alpha, 2, 1) ; 10.647972
  Precision: 5.39e-06
  match ;   Polarizability YY w=0.1     ; LINEFIELD(em_resp/freq_0.1000/alpha, 3, 2) ; 10.770509
  Precision: 5.46e-06
  match ;   Polarizability ZZ w=0.1     ; LINEFIELD(em_resp/freq_0.1000/alpha, 4, 3) ; 10.923271
  Precision: 5.10e-10
  match ;   Cross sect. (avg) w=0.1     ; LINEFIELD(em_resp/freq_0.1000/cross_section, -1, 2) ; 0.0010194335
  Precision: 3.16e-10
  match ;   Cross sect. (ani) w=0.1     ; LINEFIELD(em_resp/freq_0.1000/cross_section, -1, 3) ; 0.000631527
  Precision: 2.47e-05
  match ;   Born charge O XX w=0.0      ; LINEFIELD(em_resp/freq_0.0000/born_charges, 5, 1) ; -0.494112
  Precision: 1.71e-05
  match ;   Born charge O YY w=0.0      ; LINEFIELD(em_resp/freq_0.0000/born_charges, 6, 2) ; -0.342295
  Precision: 3.12e-04
  match ;   Born charge O ZZ w=0.0      ; LINEFIELD(em_resp/freq_0.0000/born_charges, 7, 3) ; -0.62434
  Precision: 1.45e-05
  match ;   Born charge diff w=0.0      ; GREPFIELD(em_resp/freq_0.0000/born_charges, "Discrepancy", 3, 5) ; 0.029021000000000005
  Precision: 2.66e-04
  match ;   Born charge O XX w=0.1      ; LINEFIELD(em_resp/freq_0.1000/born_charges, 5, 1) ; -0.53115
  Precision: 1.88e-05
  match ;   Born charge O YY w=0.1      ; LINEFIELD(em_resp/freq_0.1000/born_charges, 6, 2) ; -0.376688
  Precision: 3.39e-05
  match ;   Born charge O ZZ w=0.1      ; LINEFIELD(em_resp/freq_0.1000/born_charges, 7, 3) ; -0.677152
  Precision: 5.77e-06
  match ;   Born charge diff w=0.1      ; GREPFIELD(em_resp/freq_0.1000/born_charges, "Discrepancy", 3, 5) ; -0.011542
else
  match ; Error no libxc_fxc ; GREPCOUNT(err, 'not compiled with the fxc support') ; 1
endif

Input: 06-h2o_pol_lr.04_emresp_mo.inp
if(available libxc_fxc); then
  Precision: 7.85e-06
  match ;   Susceptibility XX             ; LINEFIELD(em_resp/freq_0.0000/susceptibility, 2, 1) ; -156.901971
  Precision: 7.97e-06
  match ;   Susceptibility YY             ; LINEFIELD(em_resp/freq_0.0000/susceptibility, 3, 2) ; -159.489125
  Precision: 7.87e-06
  match ;   Susceptibility ZZ             ; LINEFIELD(em_resp/freq_0.0000/susceptibility, 4, 3) ; -157.463428
  Precision: 7.90e-06
  match ;   Susceptibility av             ; LINEFIELD(em_resp/freq_0.0000/susceptibility, 5, 3) ; -157.951508
  Precision: 5.32e-06
  match ;   Polarizability XX w=0.1      ; LINEFIELD(em_resp/freq_0.1000/alpha, 2, 1) ; 10.648064
  Precision: 5.39e-06
  match ;   Polarizability YY w=0.1      ; LINEFIELD(em_resp/freq_0.1000/alpha, 3, 2) ; 10.770464
  Precision: 5.46e-06
  match ;   Polarizability ZZ w=0.1      ; LINEFIELD(em_resp/freq_0.1000/alpha, 4, 3) ; 10.923093999999999
  Precision: 5.10e-10
  match ;   Cross-section  av w=0.1      ; LINEFIELD(em_resp/freq_0.1000/cross_section, 3, 2) ; 0.0010191823
  Precision: 3.64e-09
  match ;   Mag.-opt. Re alpha w=0.1     ; GREPFIELD(em_resp/freq_0.1000/alpha_mo, 'Re alpha', 4) ; -0.0072886337
  Precision: 1.21e-08
  match ;   Mag.-opt. av Im w=0.1        ; GREPFIELD(em_resp/freq_0.1000/alpha_mo, 'Im alpha', 7) ; 0.024162072000000003
  Precision: 4.13e-12
  match ;   Mag.-opt. Re eps w=0.1       ; GREPFIELD(em_resp/freq_0.1000/alpha_mo, 'Re epsilon', 8) ; -7.82432395e-06
  Precision: 1.27e-11
  match ;   Mag.-opt. Im eps w=0.1       ; GREPFIELD(em_resp/freq_0.1000/alpha_mo, 'Im epsilon', 9) ; 2.5356062000000002e-05
else
  match ; Error no libxc_fxc ; GREPCOUNT(err, 'not compiled with the fxc support') ; 1
endif

Input: 06-h2o_pol_lr.05_emresp_susc.inp
if(available libxc_fxc); then
  Precision: 1.57e-12
  match ;  Susceptibility XX            ; LINEFIELD(em_resp/freq_0.0000/susceptibility, 2, 1) ; -156.90212400000001
  Precision: 7.97e-06
  match ;  Susceptibility YY            ; LINEFIELD(em_resp/freq_0.0000/susceptibility, 3, 2) ; -159.489064
  Precision: 7.87e-06
  match ;  Susceptibility ZZ            ; LINEFIELD(em_resp/freq_0.0000/susceptibility, 4, 3) ; -157.463394
  Precision: 7.90e-06
  match ;  Susceptibility av            ; LINEFIELD(em_resp/freq_0.0000/susceptibility, 5, 3) ; -157.951527
else
  match ; Error no libxc_fxc ; GREPCOUNT(err, 'not compiled with the fxc support') ; 1
endif
