target_sources(Octopus_lib PRIVATE
		dispersive_medium.F90
		energy_mxll.F90
		external_densities.F90
                external_waves.F90
		hamiltonian_mxll.F90
		linear_medium.F90
		maxwell.F90
		maxwell_boundary_op.F90
		propagator_mxll.F90
		)
