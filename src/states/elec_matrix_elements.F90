!! Copyright (C) 2023 F. Troisi
!!
!! This program is free software; you can redistribute it and/or modify
!! it under the terms of the GNU General Public License as published by
!! the Free Software Foundation; either version 2, or (at your option)
!! any later version.
!!
!! This program is distributed in the hope that it will be useful,
!! but WITHOUT ANY WARRANTY; without even the implied warranty of
!! MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
!! GNU General Public License for more details.
!!
!! You should have received a copy of the GNU General Public License
!! along with this program; if not, write to the Free Software
!! Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
!! 02110-1301, USA.
!!

#include "global.h"

module elec_matrix_elements_oct_m
  use batch_oct_m
  use boundaries_oct_m
  use debug_oct_m
  use derivatives_oct_m
  use electron_space_oct_m
  use fourier_space_oct_m
  use global_oct_m
  use grid_oct_m
  use hamiltonian_elec_oct_m
  use hamiltonian_elec_base_oct_m
  use ions_oct_m
  use, intrinsic :: iso_fortran_env
  use kpoints_oct_m
  use lda_u_oct_m
  use loct_math_oct_m
  use mesh_oct_m
  use mesh_batch_oct_m
  use mesh_function_oct_m
  use messages_oct_m
  use mpi_oct_m
  use mpi_lib_oct_m
  use namespace_oct_m
  use physics_op_oct_m
  use poisson_oct_m
  use profiling_oct_m
  use projector_oct_m
  use scissor_oct_m
  use singularity_oct_m
  use space_oct_m
  use states_abst_oct_m
  use states_elec_dim_oct_m
  use states_elec_oct_m
  use wfs_elec_oct_m
  use xc_oct_m

  implicit none

  private
  public :: elec_matrix_elements_t

  type elec_matrix_elements_t
    type(grid_t), pointer        :: grid
    type(space_t), pointer       :: space
    type(states_elec_t), pointer :: states

    integer                      :: st_start !< Start state index for the calculation
    integer                      :: st_end   !< Stop state index for the calculation
  contains
    procedure :: init => elec_matrix_elements_init
    ! Momentum
    procedure :: momentum_me => elec_momentum_me
    ! Angular momentum
    procedure :: angular_momentum_me => elec_angular_momentum_me
    ! Dipole
    procedure :: delec_dipole_me, zelec_dipole_me
    generic   :: dipole_me => delec_dipole_me, zelec_dipole_me
    ! KS multipoles
    procedure :: delec_ks_multipoles_3d_me, zelec_ks_multipoles_3d_me
    generic   :: ks_multipoles_3d => delec_ks_multipoles_3d_me, zelec_ks_multipoles_3d_me
    procedure :: delec_ks_multipoles_2d_me, zelec_ks_multipoles_2d_me
    generic   :: ks_multipoles_2d => delec_ks_multipoles_2d_me, zelec_ks_multipoles_2d_me
    procedure :: delec_ks_multipoles_1d_me, zelec_ks_multipoles_1d_me
    generic   :: ks_multipoles_1d => delec_ks_multipoles_1d_me, zelec_ks_multipoles_1d_me
    ! One Body
    procedure :: delec_one_body_me, zelec_one_body_me
    generic   :: one_body_me => delec_one_body_me, zelec_one_body_me
    ! Two Body
    procedure :: delec_two_body_me, zelec_two_body_me
    generic   :: two_body_me => delec_two_body_me, zelec_two_body_me
    final :: elec_matrix_elements_finalize
  end type elec_matrix_elements_t

contains

  !> Initialize the elec_matrix_elements object
  subroutine elec_matrix_elements_init(this, grid, space, states, st_start, st_end)
    class(elec_matrix_elements_t), intent(inout) :: this !< The object to be initialized
    type(grid_t), target,          intent(in)    :: grid !< The grid of the electronic system
    class(space_t), target,        intent(in)    :: space !< The space object of the electronic system
    type(states_elec_t), target,   intent(in)    :: states !< The states object of the electronic system
    integer, optional,             intent(in)    :: st_start, st_end !< Start and stop state index for the calculation

    PUSH_SUB(elec_matrix_elements_init)

    ! Associate pointers
    this%grid => grid
    this%space => space
    this%states => states

    this%st_start = optional_default(st_start, states%st_start)
    this%st_end = optional_default(st_end, states%st_end)

    POP_SUB(elec_matrix_elements_init)
  end subroutine elec_matrix_elements_init

  subroutine elec_matrix_elements_finalize(this)
    type(elec_matrix_elements_t), intent(inout) :: this
    PUSH_SUB(elec_matrix_elements_finalize)

    POP_SUB(elec_matrix_elements_finalize)
  end subroutine elec_matrix_elements_finalize

  ! -----------------------------------------------------------------------------
  subroutine elec_momentum_me(this, kpoints, momentum)
    class(elec_matrix_elements_t),  intent(in)  :: this
    type(kpoints_t),               intent(in)  :: kpoints
    FLOAT,                         intent(out) :: momentum(:,:,:)

    PUSH_SUB(elec_momentum_me)

    if (states_are_real(this%states)) then
      call delec_momentum_me(this, kpoints, momentum)
    else
      call zelec_momentum_me(this, kpoints, momentum)
    end if
    POP_SUB(elec_momentum_me)
  end subroutine elec_momentum_me

  ! -----------------------------------------------------------------------------
  subroutine elec_angular_momentum_me(this, ll, l2)
    class(elec_matrix_elements_t),  intent(in)  :: this
    FLOAT, contiguous,              intent(out) :: ll(:, :, :) !< (st%nst, st%nik, 1 or 3)
    FLOAT, contiguous,    optional, intent(out) :: l2(:, :)    !< (st%nst, st%nik)

    PUSH_SUB(elec_angular_momentum_me)

    if (states_are_real(this%states)) then
      call delec_angular_momentum_me(this, ll, l2)
    else
      call zelec_angular_momentum_me(this, ll, l2)
    end if
    POP_SUB(elec_angular_momentum_me)
  end subroutine elec_angular_momentum_me

#include "undef.F90"
#include "complex.F90"
#include "elec_matrix_elements_inc.F90"

#include "undef.F90"
#include "real.F90"
#include "elec_matrix_elements_inc.F90"

end module elec_matrix_elements_oct_m

!! Local Variables:
!! mode: f90
!! coding: utf-8
!! End:
