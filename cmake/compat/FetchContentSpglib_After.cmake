if (NOT TARGET Spglib::fortran)
	add_library(Spglib::fortran ALIAS spglib_f08_static)
endif ()
set_package_properties(Spglib PROPERTIES
		URL https://github.com/spglib/spglib
		DESCRIPTION "C library for finding and handling crystal symmetries"
)
set_property(GLOBAL APPEND PROPERTY PACKAGES_FOUND Spglib)
get_property(_packages_not_found GLOBAL PROPERTY PACKAGES_NOT_FOUND)
list(REMOVE_ITEM _packages_not_found Spglib)
set_property(GLOBAL PROPERTY PACKAGES_NOT_FOUND ${_packages_not_found})
