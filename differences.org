Comparisons based on config.h for easybuild/spack in GitLab CI

- @Ashwin: does octopus use rpath (?)

* foss-2022a_min_min_modules
- identical, we looked at this before

* foss-2022a_min

- in spack we load too many modules
- in spack we pick up additional libraries:
  - #define HAVE_BOOST
  - #define HAVE_CGAL 1
  - #define HAVE_GDLIB 1

    /* Define if libgd uses fontconfig. */
    #define HAVE_GD_FONTCONFIG 1

    /* Define if libgd uses freetype. */
    #define HAVE_GD_FREETYPE 1

    /* Define if libgd supports gif. */
    #define HAVE_GD_GIF 1

    /* Define if libgd supports jpeg. */
    #define HAVE_GD_JPEG 1

    /* Define if libgd supports png. */
    #define HAVE_GD_PNG 1

  - #define HAVE_SPARSKIT 1
  - #define HAVE_YAML 1
    
* foss-2022a

- same gdlib differences as in foss-2022a_min
- problems with futile and psolver in spack:
  - we use the internal ISF
  - #define HAVE_COMP_ISF 1
  - /* #undef HAVE_FUTILE */
  - /* #undef HAVE_PSOLVER */
** additional information from config.log for bigdft problems
*** futile
- must link against atlab: ~undefined reference to `__yaml_output_MOD_yaml_mapping_open~
  symbol defined in ~libatalab-1.a~
*** psolver
- missing module ~at_domain.mod~, which comes from bigdft-atlab
- [old psolver interface: missing symbols; however grep finds those in some of the psolver modules (?) TODO@Ashwin]
*** atlab
- many things are missing
#+BEGIN_SRC
configure:16688: checking for bigdft-atlab
configure:16719: /opt_mpsd/linux-debian11/23b/sandybridge/spack/opt/spack/linux-debian11-x86_64_v2/gcc-10.2.1/gcc-11.3.0-5uyv23o3gjfskdf4xyx7uem35j6meh2n/bin/gfortran -o conftest -I /opt_mpsd/linux-debian11/23b/sandybridge/spack/opt/spack/li
/opt_mpsd/linux-debian11/23b/sandybridge/spack/opt/spack/linux-debian11-sandybridge/gcc-11.3.0/binutils-2.38-n7v5pzd4bru52abuw3uo7cfbzmwdyv6x/bin/ld: /opt_mpsd/linux-debian11/23b/sandybridge/spack/opt/spack/linux-debian11-sandybridge/gcc-11.
harmonics.f90:(.text+0xc23): undefined reference to `__fmpi_types_MOD_fmpi_sum'
/opt_mpsd/linux-debian11/23b/sandybridge/spack/opt/spack/linux-debian11-sandybridge/gcc-11.3.0/binutils-2.38-n7v5pzd4bru52abuw3uo7cfbzmwdyv6x/bin/ld: harmonics.f90:(.text+0xc2b): undefined reference to `__f_allreduce_MOD_mpiallred_d1'
/opt_mpsd/linux-debian11/23b/sandybridge/spack/opt/spack/linux-debian11-sandybridge/gcc-11.3.0/binutils-2.38-n7v5pzd4bru52abuw3uo7cfbzmwdyv6x/bin/ld: /opt_mpsd/linux-debian11/23b/sandybridge/spack/opt/spack/linux-debian11-sandybridge/gcc-11.
harmonics.f90:(.text+0x1b82): undefined reference to `__f_enums_MOD_nullify_f_enum'
/opt_mpsd/linux-debian11/23b/sandybridge/spack/opt/spack/linux-debian11-sandybridge/gcc-11.3.0/binutils-2.38-n7v5pzd4bru52abuw3uo7cfbzmwdyv6x/bin/ld: /opt_mpsd/linux-debian11/23b/sandybridge/spack/opt/spack/linux-debian11-sandybridge/gcc-11.
box.f90:(.text+0x782): undefined reference to `__wrapper_linalg_MOD_det_3x3'
/opt_mpsd/linux-debian11/23b/sandybridge/spack/opt/spack/linux-debian11-sandybridge/gcc-11.3.0/binutils-2.38-n7v5pzd4bru52abuw3uo7cfbzmwdyv6x/bin/ld: /opt_mpsd/linux-debian11/23b/sandybridge/spack/opt/spack/linux-debian11-sandybridge/gcc-11.
box.f90:(.text+0x17bf): undefined reference to `__module_f_malloc_MOD_f_malloci_simple'
/opt_mpsd/linux-debian11/23b/sandybridge/spack/opt/spack/linux-debian11-sandybridge/gcc-11.3.0/binutils-2.38-n7v5pzd4bru52abuw3uo7cfbzmwdyv6x/bin/ld: box.f90:(.text+0x17fa): undefined reference to `__dynamic_memory_base_MOD_i1_all'
/opt_mpsd/linux-debian11/23b/sandybridge/spack/opt/spack/linux-debian11-sandybridge/gcc-11.3.0/binutils-2.38-n7v5pzd4bru52abuw3uo7cfbzmwdyv6x/bin/ld: box.f90:(.text+0x1834): undefined reference to `__module_f_malloc_MOD_f_malloci_simple'
/opt_mpsd/linux-debian11/23b/sandybridge/spack/opt/spack/linux-debian11-sandybridge/gcc-11.3.0/binutils-2.38-n7v5pzd4bru52abuw3uo7cfbzmwdyv6x/bin/ld: box.f90:(.text+0x186f): undefined reference to `__dynamic_memory_base_MOD_i1_all'
/opt_mpsd/linux-debian11/23b/sandybridge/spack/opt/spack/linux-debian11-sandybridge/gcc-11.3.0/binutils-2.38-n7v5pzd4bru52abuw3uo7cfbzmwdyv6x/bin/ld: box.f90:(.text+0x18a9): undefined reference to `__module_f_malloc_MOD_f_malloci_simple'
/opt_mpsd/linux-debian11/23b/sandybridge/spack/opt/spack/linux-debian11-sandybridge/gcc-11.3.0/binutils-2.38-n7v5pzd4bru52abuw3uo7cfbzmwdyv6x/bin/ld: box.f90:(.text+0x18e4): undefined reference to `__dynamic_memory_base_MOD_i1_all'
/opt_mpsd/linux-debian11/23b/sandybridge/spack/opt/spack/linux-debian11-sandybridge/gcc-11.3.0/binutils-2.38-n7v5pzd4bru52abuw3uo7cfbzmwdyv6x/bin/ld: box.f90:(.text+0x191a): undefined reference to `__module_f_malloc_MOD_f_mallocli0_simple'
/opt_mpsd/linux-debian11/23b/sandybridge/spack/opt/spack/linux-debian11-sandybridge/gcc-11.3.0/binutils-2.38-n7v5pzd4bru52abuw3uo7cfbzmwdyv6x/bin/ld: box.f90:(.text+0x1955): undefined reference to `__dynamic_memory_base_MOD_b1_all'
/opt_mpsd/linux-debian11/23b/sandybridge/spack/opt/spack/linux-debian11-sandybridge/gcc-11.3.0/binutils-2.38-n7v5pzd4bru52abuw3uo7cfbzmwdyv6x/bin/ld: box.f90:(.text+0x1bf2): undefined reference to `__f_utils_MOD_f_assert'
/opt_mpsd/linux-debian11/23b/sandybridge/spack/opt/spack/linux-debian11-sandybridge/gcc-11.3.0/binutils-2.38-n7v5pzd4bru52abuw3uo7cfbzmwdyv6x/bin/ld: box.f90:(.text+0x1c88): undefined reference to `__f_utils_MOD_f_assert'
/opt_mpsd/linux-debian11/23b/sandybridge/spack/opt/spack/linux-debian11-sandybridge/gcc-11.3.0/binutils-2.38-n7v5pzd4bru52abuw3uo7cfbzmwdyv6x/bin/ld: box.f90:(.text+0x1d1e): undefined reference to `__f_utils_MOD_f_assert'
/opt_mpsd/linux-debian11/23b/sandybridge/spack/opt/spack/linux-debian11-sandybridge/gcc-11.3.0/binutils-2.38-n7v5pzd4bru52abuw3uo7cfbzmwdyv6x/bin/ld: box.f90:(.text+0x1d8c): undefined reference to `__f_utils_MOD_f_assert'
/opt_mpsd/linux-debian11/23b/sandybridge/spack/opt/spack/linux-debian11-sandybridge/gcc-11.3.0/binutils-2.38-n7v5pzd4bru52abuw3uo7cfbzmwdyv6x/bin/ld: box.f90:(.text+0x1dd4): undefined reference to `__yaml_strings_MOD_attach_ci'
/opt_mpsd/linux-debian11/23b/sandybridge/spack/opt/spack/linux-debian11-sandybridge/gcc-11.3.0/binutils-2.38-n7v5pzd4bru52abuw3uo7cfbzmwdyv6x/bin/ld: box.f90:(.text+0x1ea0): undefined reference to `__yaml_strings_MOD_attach_msg_c'
/opt_mpsd/linux-debian11/23b/sandybridge/spack/opt/spack/linux-debian11-sandybridge/gcc-11.3.0/binutils-2.38-n7v5pzd4bru52abuw3uo7cfbzmwdyv6x/bin/ld: box.f90:(.text+0x2020): undefined reference to `__yaml_strings_MOD_yaml_ivtoa'
/opt_mpsd/linux-debian11/23b/sandybridge/spack/opt/spack/linux-debian11-sandybridge/gcc-11.3.0/binutils-2.38-n7v5pzd4bru52abuw3uo7cfbzmwdyv6x/bin/ld: box.f90:(.text+0x20c8): undefined reference to `__yaml_strings_MOD_combine_strings'
/opt_mpsd/linux-debian11/23b/sandybridge/spack/opt/spack/linux-debian11-sandybridge/gcc-11.3.0/binutils-2.38-n7v5pzd4bru52abuw3uo7cfbzmwdyv6x/bin/ld: box.f90:(.text+0x2105): undefined reference to `__dictionaries_MOD_f_err_throw_c'
/opt_mpsd/linux-debian11/23b/sandybridge/spack/opt/spack/linux-debian11-sandybridge/gcc-11.3.0/binutils-2.38-n7v5pzd4bru52abuw3uo7cfbzmwdyv6x/bin/ld: box.f90:(.text+0x2172): undefined reference to `__f_utils_MOD_f_assert'
/opt_mpsd/linux-debian11/23b/sandybridge/spack/opt/spack/linux-debian11-sandybridge/gcc-11.3.0/binutils-2.38-n7v5pzd4bru52abuw3uo7cfbzmwdyv6x/bin/ld: box.f90:(.text+0x21ce): undefined reference to `__f_utils_MOD_f_assert'
/opt_mpsd/linux-debian11/23b/sandybridge/spack/opt/spack/linux-debian11-sandybridge/gcc-11.3.0/binutils-2.38-n7v5pzd4bru52abuw3uo7cfbzmwdyv6x/bin/ld: box.f90:(.text+0x2213): undefined reference to `__f_utils_MOD_f_assert'
/opt_mpsd/linux-debian11/23b/sandybridge/spack/opt/spack/linux-debian11-sandybridge/gcc-11.3.0/binutils-2.38-n7v5pzd4bru52abuw3uo7cfbzmwdyv6x/bin/ld: box.f90:(.text+0x227d): undefined reference to `__f_utils_MOD_f_assert'
/opt_mpsd/linux-debian11/23b/sandybridge/spack/opt/spack/linux-debian11-sandybridge/gcc-11.3.0/binutils-2.38-n7v5pzd4bru52abuw3uo7cfbzmwdyv6x/bin/ld: box.f90:(.text+0x22eb): undefined reference to `__f_utils_MOD_f_assert'
/opt_mpsd/linux-debian11/23b/sandybridge/spack/opt/spack/linux-debian11-sandybridge/gcc-11.3.0/binutils-2.38-n7v5pzd4bru52abuw3uo7cfbzmwdyv6x/bin/ld: /opt_mpsd/linux-debian11/23b/sandybridge/spack/opt/spack/linux-debian11-sandybridge/gcc-11.
/opt_mpsd/linux-debian11/23b/sandybridge/spack/opt/spack/linux-debian11-sandybridge/gcc-11.3.0/binutils-2.38-n7v5pzd4bru52abuw3uo7cfbzmwdyv6x/bin/ld: /opt_mpsd/linux-debian11/23b/sandybridge/spack/opt/spack/linux-debian11-sandybridge/gcc-11.
box.f90:(.text+0x246a): undefined reference to `__yaml_strings_MOD_attach_ci'
/opt_mpsd/linux-debian11/23b/sandybridge/spack/opt/spack/linux-debian11-sandybridge/gcc-11.3.0/binutils-2.38-n7v5pzd4bru52abuw3uo7cfbzmwdyv6x/bin/ld: box.f90:(.text+0x2536): undefined reference to `__yaml_strings_MOD_attach_msg_c'
/opt_mpsd/linux-debian11/23b/sandybridge/spack/opt/spack/linux-debian11-sandybridge/gcc-11.3.0/binutils-2.38-n7v5pzd4bru52abuw3uo7cfbzmwdyv6x/bin/ld: box.f90:(.text+0x26b6): undefined reference to `__yaml_strings_MOD_yaml_ivtoa'
/opt_mpsd/linux-debian11/23b/sandybridge/spack/opt/spack/linux-debian11-sandybridge/gcc-11.3.0/binutils-2.38-n7v5pzd4bru52abuw3uo7cfbzmwdyv6x/bin/ld: box.f90:(.text+0x275c): undefined reference to `__yaml_strings_MOD_combine_strings'
/opt_mpsd/linux-debian11/23b/sandybridge/spack/opt/spack/linux-debian11-sandybridge/gcc-11.3.0/binutils-2.38-n7v5pzd4bru52abuw3uo7cfbzmwdyv6x/bin/ld: box.f90:(.text+0x2799): undefined reference to `__dictionaries_MOD_f_err_throw_c'
/opt_mpsd/linux-debian11/23b/sandybridge/spack/opt/spack/linux-debian11-sandybridge/gcc-11.3.0/binutils-2.38-n7v5pzd4bru52abuw3uo7cfbzmwdyv6x/bin/ld: box.f90:(.text+0x2804): undefined reference to `__yaml_strings_MOD_attach_cli'
/opt_mpsd/linux-debian11/23b/sandybridge/spack/opt/spack/linux-debian11-sandybridge/gcc-11.3.0/binutils-2.38-n7v5pzd4bru52abuw3uo7cfbzmwdyv6x/bin/ld: box.f90:(.text+0x28d0): undefined reference to `__yaml_strings_MOD_attach_msg_c'
/opt_mpsd/linux-debian11/23b/sandybridge/spack/opt/spack/linux-debian11-sandybridge/gcc-11.3.0/binutils-2.38-n7v5pzd4bru52abuw3uo7cfbzmwdyv6x/bin/ld: box.f90:(.text+0x28fb): undefined reference to `__yaml_strings_MOD_attach_cli'
/opt_mpsd/linux-debian11/23b/sandybridge/spack/opt/spack/linux-debian11-sandybridge/gcc-11.3.0/binutils-2.38-n7v5pzd4bru52abuw3uo7cfbzmwdyv6x/bin/ld: box.f90:(.text+0x2947): undefined reference to `__f_utils_MOD_f_assert_str'
/opt_mpsd/linux-debian11/23b/sandybridge/spack/opt/spack/linux-debian11-sandybridge/gcc-11.3.0/binutils-2.38-n7v5pzd4bru52abuw3uo7cfbzmwdyv6x/bin/ld: box.f90:(.text+0x29b6): undefined reference to `__dictionaries_MOD_f_err_throw_c'
/opt_mpsd/linux-debian11/23b/sandybridge/spack/opt/spack/linux-debian11-sandybridge/gcc-11.3.0/binutils-2.38-n7v5pzd4bru52abuw3uo7cfbzmwdyv6x/bin/ld: box.f90:(.text+0x29c9): undefined reference to `__dynamic_memory_base_MOD_b1_all_free'
/opt_mpsd/linux-debian11/23b/sandybridge/spack/opt/spack/linux-debian11-sandybridge/gcc-11.3.0/binutils-2.38-n7v5pzd4bru52abuw3uo7cfbzmwdyv6x/bin/ld: box.f90:(.text+0x29fb): undefined reference to `__dynamic_memory_base_MOD_i1_all_free_multi
/opt_mpsd/linux-debian11/23b/sandybridge/spack/opt/spack/linux-debian11-sandybridge/gcc-11.3.0/binutils-2.38-n7v5pzd4bru52abuw3uo7cfbzmwdyv6x/bin/ld: /opt_mpsd/linux-debian11/23b/sandybridge/spack/opt/spack/linux-debian11-sandybridge/gcc-11.
box.f90:(.text+0x4564): undefined reference to `__f_utils_MOD_put_to_zero_double_1'
/opt_mpsd/linux-debian11/23b/sandybridge/spack/opt/spack/linux-debian11-sandybridge/gcc-11.3.0/binutils-2.38-n7v5pzd4bru52abuw3uo7cfbzmwdyv6x/bin/ld: /opt_mpsd/linux-debian11/23b/sandybridge/spack/opt/spack/linux-debian11-sandybridge/gcc-11.
domain.f90:(.text+0x160f): undefined reference to `__wrapper_linalg_MOD_det_3x3'
/opt_mpsd/linux-debian11/23b/sandybridge/spack/opt/spack/linux-debian11-sandybridge/gcc-11.3.0/binutils-2.38-n7v5pzd4bru52abuw3uo7cfbzmwdyv6x/bin/ld: /opt_mpsd/linux-debian11/23b/sandybridge/spack/opt/spack/linux-debian11-sandybridge/gcc-11.
domain.f90:(.text+0x21f3): undefined reference to `__dictionaries_base_MOD_get_child_ptr'
/opt_mpsd/linux-debian11/23b/sandybridge/spack/opt/spack/linux-debian11-sandybridge/gcc-11.3.0/binutils-2.38-n7v5pzd4bru52abuw3uo7cfbzmwdyv6x/bin/ld: domain.f90:(.text+0x22ac): undefined reference to `__f_enums_MOD_char_enum'
/opt_mpsd/linux-debian11/23b/sandybridge/spack/opt/spack/linux-debian11-sandybridge/gcc-11.3.0/binutils-2.38-n7v5pzd4bru52abuw3uo7cfbzmwdyv6x/bin/ld: domain.f90:(.text+0x22cc): undefined reference to `__dictionaries_MOD_put_value'
/opt_mpsd/linux-debian11/23b/sandybridge/spack/opt/spack/linux-debian11-sandybridge/gcc-11.3.0/binutils-2.38-n7v5pzd4bru52abuw3uo7cfbzmwdyv6x/bin/ld: domain.f90:(.text+0x2414): undefined reference to `__f_enums_MOD_int_is_enum'
/opt_mpsd/linux-debian11/23b/sandybridge/spack/opt/spack/linux-debian11-sandybridge/gcc-11.3.0/binutils-2.38-n7v5pzd4bru52abuw3uo7cfbzmwdyv6x/bin/ld: domain.f90:(.text+0x247a): undefined reference to `__dictionaries_base_MOD_get_child_ptr'
/opt_mpsd/linux-debian11/23b/sandybridge/spack/opt/spack/linux-debian11-sandybridge/gcc-11.3.0/binutils-2.38-n7v5pzd4bru52abuw3uo7cfbzmwdyv6x/bin/ld: domain.f90:(.text+0x25b1): undefined reference to `__dictionaries_MOD_put_matd'
/opt_mpsd/linux-debian11/23b/sandybridge/spack/opt/spack/linux-debian11-sandybridge/gcc-11.3.0/binutils-2.38-n7v5pzd4bru52abuw3uo7cfbzmwdyv6x/bin/ld: domain.f90:(.text+0x2607): undefined reference to `__dictionaries_base_MOD_get_child_ptr'
/opt_mpsd/linux-debian11/23b/sandybridge/spack/opt/spack/linux-debian11-sandybridge/gcc-11.3.0/binutils-2.38-n7v5pzd4bru52abuw3uo7cfbzmwdyv6x/bin/ld: domain.f90:(.text+0x283f): undefined reference to `__dictionaries_base_MOD_get_child_ptr'
/opt_mpsd/linux-debian11/23b/sandybridge/spack/opt/spack/linux-debian11-sandybridge/gcc-11.3.0/binutils-2.38-n7v5pzd4bru52abuw3uo7cfbzmwdyv6x/bin/ld: domain.f90:(.text+0x286a): undefined reference to `__dictionaries_MOD_put_double'
/opt_mpsd/linux-debian11/23b/sandybridge/spack/opt/spack/linux-debian11-sandybridge/gcc-11.3.0/binutils-2.38-n7v5pzd4bru52abuw3uo7cfbzmwdyv6x/bin/ld: domain.f90:(.text+0x289f): undefined reference to `__dictionaries_base_MOD_get_child_ptr'
/opt_mpsd/linux-debian11/23b/sandybridge/spack/opt/spack/linux-debian11-sandybridge/gcc-11.3.0/binutils-2.38-n7v5pzd4bru52abuw3uo7cfbzmwdyv6x/bin/ld: domain.f90:(.text+0x28ca): undefined reference to `__dictionaries_MOD_put_double'
/opt_mpsd/linux-debian11/23b/sandybridge/spack/opt/spack/linux-debian11-sandybridge/gcc-11.3.0/binutils-2.38-n7v5pzd4bru52abuw3uo7cfbzmwdyv6x/bin/ld: domain.f90:(.text+0x28ff): undefined reference to `__dictionaries_base_MOD_get_child_ptr'
/opt_mpsd/linux-debian11/23b/sandybridge/spack/opt/spack/linux-debian11-sandybridge/gcc-11.3.0/binutils-2.38-n7v5pzd4bru52abuw3uo7cfbzmwdyv6x/bin/ld: domain.f90:(.text+0x292a): undefined reference to `__dictionaries_MOD_put_double'
/opt_mpsd/linux-debian11/23b/sandybridge/spack/opt/spack/linux-debian11-sandybridge/gcc-11.3.0/binutils-2.38-n7v5pzd4bru52abuw3uo7cfbzmwdyv6x/bin/ld: /opt_mpsd/linux-debian11/23b/sandybridge/spack/opt/spack/linux-debian11-sandybridge/gcc-11.
domain.f90:(.text+0x2c46): undefined reference to `__yaml_strings_MOD_f_strcpy'
/opt_mpsd/linux-debian11/23b/sandybridge/spack/opt/spack/linux-debian11-sandybridge/gcc-11.3.0/binutils-2.38-n7v5pzd4bru52abuw3uo7cfbzmwdyv6x/bin/ld: domain.f90:(.text+0x2c66): undefined reference to `__dictionaries_MOD_list_container_if_key
/opt_mpsd/linux-debian11/23b/sandybridge/spack/opt/spack/linux-debian11-sandybridge/gcc-11.3.0/binutils-2.38-n7v5pzd4bru52abuw3uo7cfbzmwdyv6x/bin/ld: domain.f90:(.text+0x2ca2): undefined reference to `__dictionaries_MOD_safe_get_char'
/opt_mpsd/linux-debian11/23b/sandybridge/spack/opt/spack/linux-debian11-sandybridge/gcc-11.3.0/binutils-2.38-n7v5pzd4bru52abuw3uo7cfbzmwdyv6x/bin/ld: domain.f90:(.text+0x2f17): undefined reference to `__dictionaries_MOD_key_in_dictionary'
/opt_mpsd/linux-debian11/23b/sandybridge/spack/opt/spack/linux-debian11-sandybridge/gcc-11.3.0/binutils-2.38-n7v5pzd4bru52abuw3uo7cfbzmwdyv6x/bin/ld: domain.f90:(.text+0x2f38): undefined reference to `__dictionaries_base_MOD_get_child_ptr'
/opt_mpsd/linux-debian11/23b/sandybridge/spack/opt/spack/linux-debian11-sandybridge/gcc-11.3.0/binutils-2.38-n7v5pzd4bru52abuw3uo7cfbzmwdyv6x/bin/ld: domain.f90:(.text+0x3023): undefined reference to `__dictionaries_MOD_list_container_if_key
/opt_mpsd/linux-debian11/23b/sandybridge/spack/opt/spack/linux-debian11-sandybridge/gcc-11.3.0/binutils-2.38-n7v5pzd4bru52abuw3uo7cfbzmwdyv6x/bin/ld: domain.f90:(.text+0x305a): undefined reference to `__dictionaries_MOD_safe_get_double'
/opt_mpsd/linux-debian11/23b/sandybridge/spack/opt/spack/linux-debian11-sandybridge/gcc-11.3.0/binutils-2.38-n7v5pzd4bru52abuw3uo7cfbzmwdyv6x/bin/ld: domain.f90:(.text+0x307a): undefined reference to `__dictionaries_MOD_list_container_if_key
/opt_mpsd/linux-debian11/23b/sandybridge/spack/opt/spack/linux-debian11-sandybridge/gcc-11.3.0/binutils-2.38-n7v5pzd4bru52abuw3uo7cfbzmwdyv6x/bin/ld: domain.f90:(.text+0x30b1): undefined reference to `__dictionaries_MOD_safe_get_double'
/opt_mpsd/linux-debian11/23b/sandybridge/spack/opt/spack/linux-debian11-sandybridge/gcc-11.3.0/binutils-2.38-n7v5pzd4bru52abuw3uo7cfbzmwdyv6x/bin/ld: domain.f90:(.text+0x30d1): undefined reference to `__dictionaries_MOD_list_container_if_key
/opt_mpsd/linux-debian11/23b/sandybridge/spack/opt/spack/linux-debian11-sandybridge/gcc-11.3.0/binutils-2.38-n7v5pzd4bru52abuw3uo7cfbzmwdyv6x/bin/ld: domain.f90:(.text+0x3108): undefined reference to `__dictionaries_MOD_safe_get_double'
/opt_mpsd/linux-debian11/23b/sandybridge/spack/opt/spack/linux-debian11-sandybridge/gcc-11.3.0/binutils-2.38-n7v5pzd4bru52abuw3uo7cfbzmwdyv6x/bin/ld: domain.f90:(.text+0x3252): undefined reference to `__dictionaries_MOD_key_in_dictionary'
/opt_mpsd/linux-debian11/23b/sandybridge/spack/opt/spack/linux-debian11-sandybridge/gcc-11.3.0/binutils-2.38-n7v5pzd4bru52abuw3uo7cfbzmwdyv6x/bin/ld: domain.f90:(.text+0x34a0): undefined reference to `__f_utils_MOD_put_to_zero_double_2'
/opt_mpsd/linux-debian11/23b/sandybridge/spack/opt/spack/linux-debian11-sandybridge/gcc-11.3.0/binutils-2.38-n7v5pzd4bru52abuw3uo7cfbzmwdyv6x/bin/ld: domain.f90:(.text+0x354b): undefined reference to `__dictionaries_base_MOD_get_child_ptr'
/opt_mpsd/linux-debian11/23b/sandybridge/spack/opt/spack/linux-debian11-sandybridge/gcc-11.3.0/binutils-2.38-n7v5pzd4bru52abuw3uo7cfbzmwdyv6x/bin/ld: domain.f90:(.text+0x3560): undefined reference to `__dictionaries_MOD_get_d2vec'
/opt_mpsd/linux-debian11/23b/sandybridge/spack/opt/spack/linux-debian11-sandybridge/gcc-11.3.0/binutils-2.38-n7v5pzd4bru52abuw3uo7cfbzmwdyv6x/bin/ld: domain.f90:(.text+0x3579): undefined reference to `__dictionaries_MOD_key_in_dictionary'
/opt_mpsd/linux-debian11/23b/sandybridge/spack/opt/spack/linux-debian11-sandybridge/gcc-11.3.0/binutils-2.38-n7v5pzd4bru52abuw3uo7cfbzmwdyv6x/bin/ld: domain.f90:(.text+0x3594): undefined reference to `__dictionaries_MOD_key_in_dictionary'
/opt_mpsd/linux-debian11/23b/sandybridge/spack/opt/spack/linux-debian11-sandybridge/gcc-11.3.0/binutils-2.38-n7v5pzd4bru52abuw3uo7cfbzmwdyv6x/bin/ld: domain.f90:(.text+0x35af): undefined reference to `__dictionaries_MOD_key_in_dictionary'
/opt_mpsd/linux-debian11/23b/sandybridge/spack/opt/spack/linux-debian11-sandybridge/gcc-11.3.0/binutils-2.38-n7v5pzd4bru52abuw3uo7cfbzmwdyv6x/bin/ld: domain.f90:(.text+0x35ca): undefined reference to `__dictionaries_MOD_key_in_dictionary'
/opt_mpsd/linux-debian11/23b/sandybridge/spack/opt/spack/linux-debian11-sandybridge/gcc-11.3.0/binutils-2.38-n7v5pzd4bru52abuw3uo7cfbzmwdyv6x/bin/ld: domain.f90:(.text+0x3639): undefined reference to `__yaml_strings_MOD_yaml_dtoa'
/opt_mpsd/linux-debian11/23b/sandybridge/spack/opt/spack/linux-debian11-sandybridge/gcc-11.3.0/binutils-2.38-n7v5pzd4bru52abuw3uo7cfbzmwdyv6x/bin/ld: domain.f90:(.text+0x3749): undefined reference to `__yaml_output_MOD_yaml_warning_c'
/opt_mpsd/linux-debian11/23b/sandybridge/spack/opt/spack/linux-debian11-sandybridge/gcc-11.3.0/binutils-2.38-n7v5pzd4bru52abuw3uo7cfbzmwdyv6x/bin/ld: /opt_mpsd/linux-debian11/23b/sandybridge/spack/opt/spack/linux-debian11-sandybridge/gcc-11.
domain.f90:(.text+0x427e): undefined reference to `__f_enums_MOD_f_enum_attr'
/opt_mpsd/linux-debian11/23b/sandybridge/spack/opt/spack/linux-debian11-sandybridge/gcc-11.3.0/binutils-2.38-n7v5pzd4bru52abuw3uo7cfbzmwdyv6x/bin/ld: /opt_mpsd/linux-debian11/23b/sandybridge/spack/opt/spack/linux-debian11-sandybridge/gcc-11.
domain.f90:(.text+0x49c6): undefined reference to `__dictionaries_base_MOD_get_list_ptr'
/opt_mpsd/linux-debian11/23b/sandybridge/spack/opt/spack/linux-debian11-sandybridge/gcc-11.3.0/binutils-2.38-n7v5pzd4bru52abuw3uo7cfbzmwdyv6x/bin/ld: domain.f90:(.text+0x49f9): undefined reference to `__dictionaries_MOD_put_double'
/opt_mpsd/linux-debian11/23b/sandybridge/spack/opt/spack/linux-debian11-sandybridge/gcc-11.3.0/binutils-2.38-n7v5pzd4bru52abuw3uo7cfbzmwdyv6x/bin/ld: domain.f90:(.text+0x4a17): undefined reference to `__dictionaries_base_MOD_get_list_ptr'
/opt_mpsd/linux-debian11/23b/sandybridge/spack/opt/spack/linux-debian11-sandybridge/gcc-11.3.0/binutils-2.38-n7v5pzd4bru52abuw3uo7cfbzmwdyv6x/bin/ld: domain.f90:(.text+0x4a31): undefined reference to `__dictionaries_MOD_put_value'
/opt_mpsd/linux-debian11/23b/sandybridge/spack/opt/spack/linux-debian11-sandybridge/gcc-11.3.0/binutils-2.38-n7v5pzd4bru52abuw3uo7cfbzmwdyv6x/bin/ld: /opt_mpsd/linux-debian11/23b/sandybridge/spack/opt/spack/linux-debian11-sandybridge/gcc-11.
domain.f90:(.text+0x4de6): undefined reference to `__dictionaries_base_MOD_get_list_ptr'
/opt_mpsd/linux-debian11/23b/sandybridge/spack/opt/spack/linux-debian11-sandybridge/gcc-11.3.0/binutils-2.38-n7v5pzd4bru52abuw3uo7cfbzmwdyv6x/bin/ld: domain.f90:(.text+0x4e13): undefined reference to `__dictionaries_MOD_get_value'
/opt_mpsd/linux-debian11/23b/sandybridge/spack/opt/spack/linux-debian11-sandybridge/gcc-11.3.0/binutils-2.38-n7v5pzd4bru52abuw3uo7cfbzmwdyv6x/bin/ld: domain.f90:(.text+0x501e): undefined reference to `__dictionaries_base_MOD_get_list_ptr'
/opt_mpsd/linux-debian11/23b/sandybridge/spack/opt/spack/linux-debian11-sandybridge/gcc-11.3.0/binutils-2.38-n7v5pzd4bru52abuw3uo7cfbzmwdyv6x/bin/ld: domain.f90:(.text+0x504a): undefined reference to `__dictionaries_MOD_get_double'
/opt_mpsd/linux-debian11/23b/sandybridge/spack/opt/spack/linux-debian11-sandybridge/gcc-11.3.0/binutils-2.38-n7v5pzd4bru52abuw3uo7cfbzmwdyv6x/bin/ld: /opt_mpsd/linux-debian11/23b/sandybridge/spack/opt/spack/linux-debian11-sandybridge/gcc-11.
domain.f90:(.text+0x6435): undefined reference to `__f_enums_MOD_int_enum'
/opt_mpsd/linux-debian11/23b/sandybridge/spack/opt/spack/linux-debian11-sandybridge/gcc-11.3.0/binutils-2.38-n7v5pzd4bru52abuw3uo7cfbzmwdyv6x/bin/ld: domain.f90:(.text+0x6479): undefined reference to `__f_enums_MOD_int_enum'
/opt_mpsd/linux-debian11/23b/sandybridge/spack/opt/spack/linux-debian11-sandybridge/gcc-11.3.0/binutils-2.38-n7v5pzd4bru52abuw3uo7cfbzmwdyv6x/bin/ld: domain.f90:(.text+0x6748): undefined reference to `__dictionaries_MOD_f_err_throw_c'
/opt_mpsd/linux-debian11/23b/sandybridge/spack/opt/spack/linux-debian11-sandybridge/gcc-11.3.0/binutils-2.38-n7v5pzd4bru52abuw3uo7cfbzmwdyv6x/bin/ld: domain.f90:(.text+0x686e): undefined reference to `__dictionaries_MOD_f_err_throw_c'
/opt_mpsd/linux-debian11/23b/sandybridge/spack/opt/spack/linux-debian11-sandybridge/gcc-11.3.0/binutils-2.38-n7v5pzd4bru52abuw3uo7cfbzmwdyv6x/bin/ld: domain.f90:(.text+0x6d65): undefined reference to `__f_utils_MOD_f_assert'
/opt_mpsd/linux-debian11/23b/sandybridge/spack/opt/spack/linux-debian11-sandybridge/gcc-11.3.0/binutils-2.38-n7v5pzd4bru52abuw3uo7cfbzmwdyv6x/bin/ld: domain.f90:(.text+0x6dec): undefined reference to `__dictionaries_MOD_f_err_throw_c'
/opt_mpsd/linux-debian11/23b/sandybridge/spack/opt/spack/linux-debian11-sandybridge/gcc-11.3.0/binutils-2.38-n7v5pzd4bru52abuw3uo7cfbzmwdyv6x/bin/ld: domain.f90:(.text+0x6e7d): undefined reference to `__dictionaries_MOD_f_err_throw_c'
/opt_mpsd/linux-debian11/23b/sandybridge/spack/opt/spack/linux-debian11-sandybridge/gcc-11.3.0/binutils-2.38-n7v5pzd4bru52abuw3uo7cfbzmwdyv6x/bin/ld: domain.f90:(.text+0x6f0a): undefined reference to `__dictionaries_MOD_f_err_throw_c'
/opt_mpsd/linux-debian11/23b/sandybridge/spack/opt/spack/linux-debian11-sandybridge/gcc-11.3.0/binutils-2.38-n7v5pzd4bru52abuw3uo7cfbzmwdyv6x/bin/ld: domain.f90:(.text+0x70fb): undefined reference to `__f_utils_MOD_f_assert'
/opt_mpsd/linux-debian11/23b/sandybridge/spack/opt/spack/linux-debian11-sandybridge/gcc-11.3.0/binutils-2.38-n7v5pzd4bru52abuw3uo7cfbzmwdyv6x/bin/ld: domain.f90:(.text+0x71d6): undefined reference to `__dictionaries_MOD_f_err_throw_c'
/opt_mpsd/linux-debian11/23b/sandybridge/spack/opt/spack/linux-debian11-sandybridge/gcc-11.3.0/binutils-2.38-n7v5pzd4bru52abuw3uo7cfbzmwdyv6x/bin/ld: /opt_mpsd/linux-debian11/23b/sandybridge/spack/opt/spack/linux-debian11-sandybridge/gcc-11.
domain.f90:(.text+0x7d5c): undefined reference to `__f_enums_MOD_enum_is_enum'
collect2: error: ld returned 1 exit status
configure:16719: $? = 1
configure: failed program was:
|       program main
|
|   use f_harmonics
|   type(f_multipoles) :: mp
|   call f_multipoles_create(mp,2)
|
|
|
|       end
configure:16726: result: no (-I /opt_mpsd/linux-debian11/23b/sandybridge/spack/opt/spack/linux-debian11-sandybridge/gcc-11.3.0/bigdft-atlab-1.9.2-qj2zmdpgtvndlmyezmdsyuhilgbycpew/include -L/opt_mpsd/linux-debian11/23b/sandybridge/spack/opt/s
#+END_SRC
- link against futile

** Results from including/linking atlab
- Futile: linking (-L) against atlab (see sh script in ci directory) does not seem to work; error is persistent
- PSolver: including (-I) atlab and futile results in a new problem conftest.f90:(.text+0x68): undefined reference to ~__pstypes_MOD_pkernel_init~
  grepping for this symbol shows it in ~libPSolver-1.a~, which should be in the linker line (?)

* foss-2022a_debug
- identical to foss-2022a

* foss-2022a_omp
- identical to foss-2022a

* foss-2022a_opt
- identical to foss-2022a

* foss-2022a_mpi_min
- different compiler? easybuild mpifort; spack mpif90; Henning says most likely not
- spack has too many libraries, we pick up
  - boost
  - cgal
  - gdlib
  - parmetis
  - sparskit
  - yaml
  - parmetis instead of (?) metis (I think)
    /* This is defined when we link with the internal METIS library (default). */
    /* #undef HAVE_COMP_METIS */
  - different METIS_IDXTYPEWIDTH 23 (eb) <-> 64 (spack)
- spack has #define HAVE_FFTW3_MPI 1; Henning: maybe not MPI support in easybuild? does configure have an option to disable MPI in FFTW?

* foss-2022a_mpi_min_min_modules
- spack provides FFTW with MPI support EasyBuild without; what is desired?
  ~#define HAVE_FFTW3_MPI 1~

* foss-2022a_mpi
- missing
  - berkeleygw (/* #undef HAVE_BERKELEYGW */)
    from config.log: ~f951: Warning: Nonexistent include directory \~
    ~'/opt_mpsd/linux-debian11/23b/sandybridge/spack/opt/spack/linux-debian11-sandybridge/gcc-11.3.0/berkeleygw-2.1-efnk6ysoris2ljbtgzhn4wewdnmaorjz/library' [-Wmissing-include-dirs]~
    **The same happens for the serial toolchain for spack and EasyBuild**
    in Spack the BerkelyGW package has the usual ~bin~, ~include~, ~lib~, ~share~
  - futile/psolver (as above)
  - elpa (???)
    /* Define if ELPA is available with openMP support */
    /* #undef HAVE_ELPA */
    [I thought the request from the Octopus people was to build elpa without openMP support? TODO@Ashwin]
    [we build elpa with openMP support elpa@2021.11.001%gcc@11.3.0~autotune~cuda+mpi+openmp~rocm build_system=autotools arch=linux-debian11-sandybridge]
    ~configure:16238: WARNING: Octopus is compiled without openMP but elpa_openmp library is requested, skipping elpa inorder to avoid bringing in openMP~
- spack has #define HAVE_FFTW3_MPI 1
- additional in spack
  - pfft
  - pnfft
    they are requested in eb but seem not to be picked up (?); I could not find any output in config.log for eb
    The buildbot behaves the same; for spack I find ~checking for pfft~ and ~checking for pnfft~
    ["--with-pfft-prefix=$EBROOTPFFT" "--with-pnfft-prefix=$EBROOTPNFFT"]

* foss-2022a_mpi_debug
- different FCFLAGS (buildbot deviates from the "standard", my current spack jobs do not reflect this)
- max-dim 4 (eb) <-> 3 (spack) re-ckeck in the buildbot (!!!)
- same as foss-2022a_mpi

* foss-2022a_mpi_omp
- similar to foss-2022a_mpi
- DIFFERENCE: elpa is picked up (openMP)

* foss-2022a_mpi_opt
- different CXX optimisation level (buildbot is inconsistent)
- similar to foss-2022a_mpi
