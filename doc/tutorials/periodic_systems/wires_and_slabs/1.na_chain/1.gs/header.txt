    <><><><><><><><><><><><><><><><><><><><><><><><><><><><><><><>
                                ___
                             .-'   `'.
                            /         \
                            |         ;
                            |         |           ___.--,
                   _.._     |0) ~ (0) |    _.---'`__.-( (_.
            __.--'`_.. '.__.\    '--. \_.-' ,.--'`     `""`
           ( ,.--'`   ',__ /./;   ;, '.__.'`    __
           _`) )  .---.__.' / |   |\   \__..--""  """--.,_
          `---' .'.''-._.-'`_./  /\ '.  \ _.-~~~````~~~-._`-.__.'
                | |  .' _.-' |  |  \  \  '.               `~---`
                 \ \/ .'     \  \   '. '-._)
                  \/ /        \  \    `=.__`~-.
             jgs  / /\         `) )    / / `"".`\
            , _.-'.'\ \        / /    ( (     / /
             `--~`   ) )    .-'.'      '.'.  | (
                    (/`    ( (`          ) )  '-;
                     `      '-;         (-'

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2, or (at your option)
    any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA

    <><><><><><><><><><><><><><><><><><><><><><><><><><><><><><><>

                           Running octopus

Version                : bimaculatus
Commit                 : 865262acca2f58a8c1865f3eacd24f259041f926
Configuration time     : Tue Jun  6 13:47:35 CEST 2023
Configuration options  : maxdim3 sse2 avx libxc5 libxc_fxc
Optional libraries     : cgal nfft pspio
Architecture           : x86_64
C compiler             : gcc
C compiler flags       : -Wall -O2 -march=native -fprofile-arcs -ffpe-trap=zero,invalid
C++ compiler           : g++
C++ compiler flags     : -Wall -O2 -march=native -fprofile-arcs -ffpe-trap=zero,invalid
Fortran compiler       : gfortran (GCC version 10.3.0)
Fortran compiler flags : -g -Wall -fno-var-tracking-assignments -Wno-maybe-uninitialized -Wno-unused-dummy-argument -Wno-c-binding-type -Wno-surprising -Wno-maybe-uninitialized -O2 -march=native -fbacktrace -fprofile-arcs -ffpe-trap=zero,invalid -finit-int

             The octopus is swimming in poppyseed (Linux)


            Calculation started on 2023/06/07 at 15:09:42
